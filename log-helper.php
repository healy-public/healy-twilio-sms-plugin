<?php

namespace Crux\WordPress;

function log($msg, string $prefix = ''): void
{
    if (is_array($msg) || is_object($msg)) {
        error_log($prefix . json_encode($msg));
        //error_log(print_r($msg, true));
    } else {
        error_log($prefix . $msg);
    }
}

function dbg($msg): void
{
    if (defined('WP_DEBUG') && WP_DEBUG) {
        log($msg);
    }
}

function err($msg): void
{
    log($msg, '#### ');
}
