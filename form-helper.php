<?php

declare(strict_types=1);

namespace Crux\WordPress\FormHelper;

require_once __DIR__ . '/log-helper.php';
use function Crux\WordPress\{ log, err, dbg };

require_once __DIR__ . '/config-helper.php';
use function Crux\WordPress\Config\get as plugin_config;
use function Crux\WordPress\Config\get_val as field_val;

function init(string $form_name): void
{
    dbg("initializing form helper({$form_name})");
    install_save_action($form_name);
}

function render_input(string $form_name, string $label, string $key): void { ?>
    <tr>
        <td>
            <label for="<?php echo $key ?>"><?php echo $label; ?>:</label>
        </td>
        <td>
            <input
                id="<?php echo $key ?>"
                name="<?php echo $form_name ?>[<?php echo $key ?>]"
                class="regular-text" type="text"
                value="<?php echo field_val($form_name, $key) ?>"
            />
        </td>
    <tr>
<?php }

function form(string $form_name): object
{
    static $forms = [];

    $form = $forms[$form_name] ?? null;
    if ($form === null) {
        $form = $forms[$form_name] = (object)[];
    }

    return $form;
}

function install_save_action(string $form_name): void
{
    $slug = action_slug($form_name);
    add_action("admin_post_{$slug}", fn () => save_action($form_name, $_POST[$form_name]));
}

function action_slug(string $form_name): string
{
    $sanitized_from_name = preg_replace('/[-\s]/', '_', $form_name);
    return "{$sanitized_from_name}_settings";
}

function add_save_action_callback(string $form_name, callable $f): void
{
    form($form_name)->save_action_callback = $f;
}

function save_action_callback(string $form_name): ?callable
{
    return form($form_name)->save_action_callback ?? null;
}

// catches the form posted in 'render_settings_markup' call with the hidden 'action' input:
//
//  <input type="hidden" name="action" value="update_healy_twilio_settings" />
function save_action(string $form_name, array $fields): void
{
    $fields = stripslashes_deep($fields);
    dbg([
        'save_action' => $form_name,
        'fields' => $fields
    ]);
    update_option($form_name, $fields, true);

    $cb = save_action_callback($form_name);
    if ($cb) {
        $cb($fields); // TODO: receive status feedback from callback?
    }

    // TODO: maybe contain this whole function in a try/catch block?

    // somehow the status param on the redirect to signal user notifications.
    // $settings_page_url = "/wp-admin/options-general.php?page=healy-oidc&status=success";
    // add_settings_error('healy-twilio-sms', 'healy-twilio-sms-' . $ex->getCode(), $ex->getMessage(), 'error');
    header("Location: " . wp_get_referer());
    exit;
}
