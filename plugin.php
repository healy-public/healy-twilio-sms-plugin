<?php

declare(strict_types=1);

namespace Healy\Twilio\Sms\Plugin;

use Twilio\Rest\Client;

require_once __DIR__ . '/log-helper.php';
use function Crux\WordPress\{ log, err, dbg };

require_once __DIR__ . '/config-helper.php';
use function Crux\WordPress\Config\get as plugin_config;
use function Crux\WordPress\Config\get_enved_val;

const PLUGIN_NAME = 'healy-twilio-sms';

add_action('init', function (): void {
    dbg('plugin init: ' . __FILE__);
    dbg(['config' => plugin_config(PLUGIN_NAME)]);

    add_action(
        'send-healy-twilio-sms',
        fn (string $number, string $message) => send_sms($number, $message),
        10, 2
    );
});

function send_sms(string $receiver_number, string $txt): void
{
    //$c = plugin_config(PLUGIN_NAME);
    // dbg(['sending SMS' => [
    //     'receiver #no' => $receiver_number,
    //     'message' => $txt,
    //     'Account SID' => $c->account_sid ?? '',
    //     'Auth Token' => $c->auth_token ?? '',
    //     "HEALY_TWILIO_SMS_ACCOUNT_SID" => config('account_sid'),
    //     "HEALY_TWILIO_SMS_AUTH_TOKEN" => config('auth_token'),
    //     "HEALY_TWILIO_SMS_SENDER_NO" => config('sender_no'),
    // ]]);
    //
    try {
        twilio_send_sms($receiver_number, $txt);
    } catch (\Exception $ex) {
        err("failed to send SMS: " . $ex->getMessage());
        dbg(['callstack' => $ex->getTraceAsString()]);
    }
}

function twilio_send_sms(string $receiver_number, string $txt): void
{
    $twilio = new Client(config('account_sid'), config('auth_token'));
    $twilio->messages->create($receiver_number, ['from' => config('sender_no'), 'body' => $txt]);
}

function config(string $key): ?string
{
    return get_enved_val(PLUGIN_NAME, $key);
}
