<?php

declare(strict_types=1);

namespace Crux\WordPress\Config;

// overloading the wordpress get_option method in the namespace of the to be
// tested module
function get_option(string $key, $default = null) /* :mixed */
{
    return Test\ConfigTest::options($key) ?? $default ?? false;
}

namespace Crux\WordPress\Config\Test;

require_once __DIR__ . '/../../log-helper.php';
use function Crux\WordPress\{ log, err, dbg };

require_once __DIR__ . '/../../config-helper.php';
use function Crux\WordPress\Config\{
    caching as cache_config,
    get as get_config,
    get_val as get_config_val,
    enved_key,
    get_enved_val,
};

use Healy\Twilio\Sms\Plugin\Test\PluginTestCase;

// tested for in test_get_enved_val_constant() test
define('TEST_CONSTANT', '1611');

final class ConfigTest extends PluginTestCase
{
    public static $options = null;

    public function setUp(): void
    {
        cache_config(false);
        self::$options = [];
    }

    public function tearDown(): void
    {
        self::$options = [];
    }

    public static function options(string $key) /* :mixed */
    {
        return self::$options[$key] ?? null;
    }

    public function test_get_returns_default_empty(): void
    {
        $this->assertEquals((object)[], get_config('config-helper'));
    }

    public function test_get_keeps_configs_separated(): void
    {
        self::$options = ['yada' => ['k' => 'v']];
        $this->assertEquals((object)[], get_config('nada'));
    }

    public function test_get_with_options(): void
    {
        self::$options = ['config' => ['key' => 'yalla']];
        $this->assertEquals((object)['key' => 'yalla'], get_config('config'));
    }

    public function test_get_returns_object(): void
    {
        self::$options = ['config-helper' => ['key' => 'yalla']];
        $this->assertEquals('yalla', get_config('config-helper')->key);
    }

    public function test_enved_key(): void
    {
        $this->assertEquals('FOO_BAR_BOO', enved_key('foo-bar', 'boo'));
        $this->assertEquals('FOO_BAR_BOO_BAR', enved_key('foo-bar', 'boo bar'));
        $this->assertEquals('FOO___BAR_BOO___BAR', enved_key('foo---bar', 'boo   bar'));
        $this->assertEquals('__FOO_BAR', enved_key('__foo', 'bar'));
        $this->assertEquals('__FOO___BAR', enved_key('__foo', '__bar'));
        $this->assertEquals('_FOO___BAR_', enved_key(' foo ', ' bar '));
    }

    public function test_get_val(): void
    {
        self::$options = ['test' => ['key' => 'yalla']];
        $this->assertEquals('yalla', get_config_val('test', 'key'));
    }

    public function test_get_enved_val_default(): void
    {
        $this->assertEquals('23', get_enved_val('config-helper', 'none existing', '23'));
    }

    public function test_get_enved_val_option(): void
    {
        self::$options = ['test' => ['key' => 'yalla']];
        $this->assertEquals('yalla', get_enved_val('test', 'key', '23'));
    }

    public function test_get_enved_val_constant(): void
    {
        // see: define('TEST_CONSTANT', '1611');
        $this->assertEquals('1611', get_enved_val('test', 'constant', '23'));
    }

    public function test_get_val_option_overrides_constant(): void
    {
        self::$options = ['test' => ['constant' => '-1']];
        // see: define('TEST_CONSTANT', '1611');
        $this->assertEquals('-1', get_config_val('test', 'constant'));
    }

    public function test_get_enved_val_overrides_option_and_constant(): void
    {
        // see: define('TEST_CONSTANT', '17');
        self::$options = ['test' => ['constant', 'yada yada']];
        putenv('TEST_CONSTANT=hey!');
        $this->assertEquals('hey!', get_enved_val('test', 'constant', '23'));
    }
}
