<?php

declare(strict_types=1);

namespace Crux\WordPress\Config;

require_once __DIR__ . '/log-helper.php';
use function Crux\WordPress\{ log, err, dbg };

function get_val(string $name, string $key, ?string $default = null) // :mixed
{
    return get($name)->{$key} ?? $default;
}

// implement the following precedence to resolve config variables:
//
//        #    ||   env  |  WordPress DB | constant  ||  return
//     --------++--------+---------------+-----------++--------
//     0  b000 ||  null  |      null     |   null    ||  default
//     2  b001 ||        |               |  '---'    ||  '---'
//     1  b010 ||        |     'ooo'     |           ||  'ooo'
//     3  b011 ||        |     'ooo'     |  '---'    ||  'ooo'
//     4  b100 || '+++'  |               |           ||  '+++'
//     6  b101 || '+++'  |               |  '---'    ||  '+++
//     5  b110 || '+++'  |     'ooo'     |           ||  '+++'
//     7  b111 || '+++'  |     'ooo'     |  '---'    ||  '+++
//
function get_enved_val(string $name, string $key, ?string $default = null) // :mixed
{
    $ekey = enved_key($name, $key);
    // ENV  -->  WordPress  -->  PHP Constant  -->  Default value
    return getenv($ekey) ?: (get_val($name, $key) ?: (defined($ekey) ? constant($ekey) : $default));
}

// the base function, getting a full config options array from the wordpress database.
function get(string $key): object
{
    static $cache = [];

    $config = $cache[$key] ?? null;
    if (!caching() || ($config === null)) {
        //dbg("loading config({$key})");
        $config = $cache[$key] = (object) get_option($key, []);
    }

    return $config;
}

function caching(?bool $flg = null): bool
{
    static $caching = true;
    if (null !== $flg) {
        $caching = $flg;
    }

    return $caching;
}

function enved_key(string $group, string $key): string
{
    return preg_replace('/[-\s]/', '_', strtoupper("${group}_${key}"));
}
